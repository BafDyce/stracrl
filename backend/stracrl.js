// stracrl.js
var google = require('googleapis');
var async = require('async');
var logger = require('./logger.js');

function Stracrl(config){
    this.lastUpdated = 0;
    this.updateInProgress = false;
    this.gt3drivers = {};
    this.gtedrivers = {};
    this.key = config.key;
    var data = require('./data.json');
    this.spreadSheetId = data.spreadSheetId;
    this.sheets = data.sheets;
}

Stracrl.prototype.updateIfNeccessary = function(callback){
    var now = Date.now();
    // 86 400 000 ms are one day
    if( now - this.lastUpdated > 86400000){
        if ( this.isUpdateInProgress() === true ){
            // wait until update is finished
            var self = this;
            async.whilst(
                // while an update is in progress
                // directly passing self.isUpdateInProgress does not work. Don't know why.
                function(){ return self.isUpdateInProgress(); },
                // wait 100 ms
                function(callback){ setTimeout(callback, 100); },
                // called when update finished
                function(err){
                    callback();
                }
            );
        } else {
            this.update(callback);
        }
        return;
    }

    callback();
}

Stracrl.prototype.isUpdateInProgress = function(){
    return this.updateInProgress;
}

Stracrl.prototype.update = function(callback){
    this.updateInProgress = true;
    logger.info('Update initiated');
    var start = Date.now();
    var updateCalls = [];
    // we need 'self', because the reference to 'this' is lost in async.parallel()
    var self = this;

    updateCalls.push( function(callback){
        self.importDriverlist(self, "gt3", "eu", "pro", function(){
            // the following call to callback is required by async.parallel()
            callback(null, 'gt3eupro');
        });
    });

    updateCalls.push( function(callback){
        self.importDriverlist(self, "gt3", "eu", "pro-am", function(){
            callback(null, 'gt3euproam');
        });
    });

    updateCalls.push( function(callback){
        self.importDriverlist(self, "gt3", "eu", "am", function(){
            callback(null, 'gt3euam');
        });
    });

    updateCalls.push( function(callback){
        self.importDriverlist(self, "gt3", "na", undefined, function(){
            callback(null, 'gt3na');
        });
    });

    updateCalls.push( function(callback){
        self.importDriverlist(self, "gte", "eu", undefined, function(){
            callback(null, 'gteeu');
        });
    });

    updateCalls.push( function(callback){
        self.importDriverlist(self, "gte", "na", undefined, function(){
            callback(null, 'gtena');
        });
    });

    async.parallel(updateCalls, function(err, result){
        if (err){
            // we shouldn't really throw any errors, but maybe just an empty response
            throw err;
        }
        // util.inspect(this.drivers, {showHidden: false, depth: null});
        self.lastUpdated = Date.now();
        var ms = (self.lastUpdated - start);
        self.updateInProgress = false;
        logger.info("Update finished in " + ms + " ms");
        callback();
    });
}

Stracrl.prototype.importDriverlist = function(self, series, region, division, callback){
    self.loadDriverlistFromSheet(self, series, region, division, function(drivernames){
        // save driverlist
        var driverlist = {};
        if (series !== undefined){
            if ( series === "gt3"){
                driverlist = self.gt3drivers;
            } else if ( series === "gte"){
                driverlist = self.gtedrivers;
            } else {
                return callback();
            }
        } else {
            return callback();
        }

        var length = drivernames.length;
        for (var ii = 0; ii < length; ii++){
            //self.gt3drivers[drivernames[ii]] = {series: "gt3", region: "eu", division: "pro"};
            var element = {};
            element['series'] = (series !== undefined ? series : 'unknown');
            element['region'] = (region !== undefined ? region : 'unknown');
            element['division'] = (division !== undefined ? division : 'unknown');
            driverlist[drivernames[ii]] = element;
        }
        callback();
    });
}

Stracrl.prototype.loadDriverlistFromSheet = function(self, series, region, division, callback){
    var sheetInfo = self.getSheetInfo(series, region, division);
    if( sheetInfo !== undefined){
        var sheets = google.sheets('v4');
        sheets.spreadsheets.values.get({
            key: self.key,
            spreadsheetId: self.spreadSheetId,
            range: sheetInfo.sheetId + '!' + sheetInfo.range,
        }, function(err, response) {
            if (err) {
                logger.error('Sheets-API returned an error: ' + err);
                return;
            }
            var rows = response.values;
            if (rows.length != 0) {
                callback(rows);
            } else {
                logger.warn('Sheets-API didn\'t return data');
            }
        });
    }
}

Stracrl.prototype.getSheetInfo = function(series, region, division){
    if (series !== undefined && this.sheets.hasOwnProperty(series)){
        var s = this.sheets[series];
        if (region !== undefined && s.hasOwnProperty(region)){
            var r = s[region];
            if (division !== undefined){
                if (r.hasOwnProperty(division)){
                    return r[division];
                }
            } else {
                // no division requested, assume no division exists
                return r;
            }
        }
    }

    return undefined;
}

Stracrl.prototype.getSingleDriverInfos = function(self, name, callback){
    var match = false;
    var info = self.gt3drivers[name];
    if( info !== undefined ){
        callback({name: name,
            series: info.series,
            region: info.region,
            division: info.division});
        match = true;
    }

    info = self.gtedrivers[name];
    if ( info !== undefined ){
        callback({name: name,
            series: info.series,
            region: info.region,
            division: info.division});
        match = true;
    }

    if ( match ){
        return;
    }

    callback({name: name, series: 'unknown', region: 'unknown', division: 'unknown'});
}

Stracrl.prototype.getDriverInfos = function(drivers, callback){
    var self = this;
    this.updateIfNeccessary( function() {
        var result = [];
        var length = drivers.length;
        for (var ii = 0; ii < length; ii++){
            self.getSingleDriverInfos(self, drivers[ii], function(res) {
                result.push(res);
            })
        }

        callback(result);
    });
}

module.exports = Stracrl;

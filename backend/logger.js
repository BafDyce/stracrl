var winston = require('winston');

var logger = new (winston.Logger)({
    level: 'info',
    transports: [
        new (winston.transports.Console)({json: false, timestamp: true}),
        new (require('winston-daily-rotate-file'))({filename: './logs/stracrl-log'})
    ]
});

module.exports = logger;

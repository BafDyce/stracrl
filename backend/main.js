const util = require('util');
var express = require('express');
var app = express();

var logger = require('./logger.js');
var config = require('./config.json');
const Stracrl = require('./stracrl.js');
var stracrl = new Stracrl(config);

// for parsing application/json
var bodyParser = require('body-parser');
app.use(bodyParser.json());

// for fast copy & paste during debugging
// util.inspect(x, {showHidden: false, depth: null})

app.post('/drivers', function(req, res) {
    var start = Date.now();
    var ip = req.ip;
    logger.info('Request from ' + ip);

    // TODO: input validation & sanitization !!
    var drivers = req.body.drivers;
    if (drivers !== undefined){
        stracrl.getDriverInfos(drivers, function(result){
            res.send(result);
            var end = Date.now();
            var delta = (end - start);
            if ( delta > 100 ){
                logger.warn('Compiling reply for ' + ip + ' took ' + delta + ' ms');
            }
        });
        return;
    }

    // in case of an error also send a reply
    result = {error: "Invalid request"};
    res.send(result);
});

// we need to allow/enable Cross-Origin Ressource Sharing (CORS)
// see http://justindavis.co/2015/08/31/CORS-in-Express/ (accessed 03.09.2016)
var allowCrossDomain = function(req, res, next) {
    if ('OPTIONS' == req.method) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'POST,OPTIONS');
        res.header('Access-Control-Allow-Headers',
            'Content-Type, Authorization, Content-Length, X-Requested-With');
        res.send(200);
    }
    else {
        next();
    }
};
app.use(allowCrossDomain);

var server = app.listen(8080, function() {
    var host = server.address().address;
    var port = server.address().port;

    logger.info("Listening at http://%s:%s", host, port);
});

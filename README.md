# ![logo][logo] stracrl
stracrl is an OpenSource Firefox-Plugin which allows you to display
[ACRL](https://www.reddit.com/r/acrl) driver infos on selected
[stracker](http://n-e-y-s.de/main) pages.

## Installation
[Official Firefox AddOn-Page](https://addons.mozilla.org/en-us/firefox/addon/stracrl/)

## What it does
When browsing a stracker page which was previously added to the plugins list
of sites to run on, it displays additional driver information, relevant for
ACRL.

Currently this only includes region (EU, NA) and division (PRO, PRO-AM, AM)
if available. If you want to see more information file an issue and I'll see
what I can do.

**Note:** Distinction between PRO-AM and AM is not 100% correct yet because
for the races the distinction depends on the number if sign ups. Currently
I just put the top 35 drivers into PRO-AM and the rest into AM. I have some
ideas on how to refine this though.

## You are in control!
By default the list of enabled pages is empty. You can add/remove any pages
you like (be aware that the plugin will break non-stracker pages if you add
them to the list). The plugin will never share the list of pages in your list!

Additionally you can enabled and disable the plugin any time you want -
directly from the plugins' UI itself. So you don't have to do that in the
browsers' settings.

## How it works
When the plugin is on an enabled stracker page, it creates a list of all
driver names. Then, it queries the stracrl server over a secure HTTPS
connection, which sends back the neccessary information.

The stracrl server itself updates the data daily from the official ACRL
spreadsheets.

## Any contribution is welcome!
Whether you have an idea for a cool new feature, discovered a bug or
otherwise want to help, you can do so.

The main repository is located at
[gitlab.com](https://gitlab.com/BafDyce/stracrl) and I prefer to have
everything there. However, if you don't have a gitlab Account (and don't)
want to create one for free you can also use the repo at
[github.com](https://github.com/BafDyce/stracrl).

## Screenshots & Video
![lapstats](./screenshots/lapstats.png)
![panel](./screenshots/panel-numbered.png)
1. Open plugins UI
2. Quickly turn on and off the plugin
3. Add/Remove current page
4. List of all pages on which the plugin will run
5. Click on the address to open in a new tab
6. Remove the page from the list without having to visit it

Video:
[Demonstration of the stracrl browser plugin](https://www.youtube.com/watch?v=UfAcI5wvSNs)

## Acknowledgments
* Graphics are either taken from [openclipart.org](https://openclipart.org/) or
from the ACRL wiki.
*  /r/acrl for being an awesome community.
* Monster Energy for supporting my nightly programming sessions
* Let's Encrypt for being awesome.
* **You** for using stracrl!

[logo]: logo.png

var { ToggleButton } = require('sdk/ui/button/toggle');
var panels = require("sdk/panel");
var self = require("sdk/self");
var tabs = require("sdk/tabs");
var pageMod = require("sdk/page-mod");
const ConfigurationHandler = require('./data/configurationHandler.js');
var configurationHandler = new ConfigurationHandler();
const backendCommunicator = require('./data/backendCommunicator.js');
const utilities = require('./data/utilities.js');

var button = ToggleButton({
  id: "stracrl",
  label: "stracrl",
  icon: {
    "16": "./images/icon-16.png",
    "32": "./images/icon-32.png",
    "48": "./images/icon-48.png",
    "64": "./images/icon-64.png"
  },
  onChange: handleChange
});

var panel = panels.Panel({
  contentURL: self.data.url("./panel.html"),
  width: 400,
  onHide: handleHide
});

panel.port.on('panelGetConfig', function(){
    panel.port.emit('panelGetConfigReply', configurationHandler.getConfig());
});

panel.port.on('panelStateNotice', function(state){
    configurationHandler.setOnOffState(state);
});

panel.port.on('panelAddRemovePage', function(data){
    if ( data.hasOwnProperty('url') && data.hasOwnProperty('mode') ){
        var success = false;
        if ( data.mode === 'remove' ){
            success = removePage(data.url);
        } else if ( data.mode === 'add' ) {
            success = addPage(data.url);
        }

        var enabledPages = configurationHandler.getEnabledPages();
        var reply = {success: success, enabledPages: enabledPages, mode: data.mode};
        panel.port.emit('panelAddRemovePageReply', reply);
    }
});

panel.port.on('panelOpenPage', function(url){
    tabs.open(url);
});

// register script to be called whenever a new page is loaded
pageMod.PageMod({
    include: "*",
    contentScriptFile: [
        self.data.url("./utilities.js"),
        self.data.url("./tabWorker.js")
    ],
    contentScriptOptions: {
        imgRegionEu: self.data.url('./images/region-eu.png'),
        imgRegionNa: self.data.url('./images/region-na.png'),
        imgGt3Pro: self.data.url('./images/gt3-pro.png'),
        imgGt3ProAm: self.data.url('./images/gt3-pro-am.png'),
        imgGt3Am: self.data.url('./images/gt3-am.png'),
        imgGte: self.data.url('./images/gte.png')
    },
    onAttach: function(worker){
        worker.port.on("workerDriversLookup", function(drivernames){
            backendCommunicator.driverLookup(drivernames, function(response){
                worker.port.emit("workerDriversLookupReply", response);
            });
        });
        worker.port.on('workerGetConfig', function(){
            var config = configurationHandler.getConfig();
            worker.port.emit('workerGetConfigReply', config);
        });
        worker.port.on('workerOpenPage', function(url){
            sendPanelOpenPage(url);
        });
    }
});

// register notification when active tab changes
tabs.on('activate', function () {
    sendPanelOpenPage(tabs.activeTab.url);
});

// END OF MAIN
function sendPanelOpenPage(url){
    var baseUrl = utilities.urlToBaseUrl(url);
    var enabledPages = configurationHandler.getEnabledPages();
    var data = {url: baseUrl, enabledPages: enabledPages};
    panel.port.emit('panelOpenPageChange', data);
}

function handleChange(state) {
  if (state.checked) {
    panel.show({
      position: button
    });
  }
}

function handleHide() {
  button.state('window', {checked: false});
}

function removePage(pageToRemove){
    var url = utilities.urlToBaseUrl(pageToRemove);
    var success = configurationHandler.removePage(url);
    return success;
}

function addPage(pageToAdd){
    var url = utilities.urlToBaseUrl(pageToAdd);
    var success = configurationHandler.addPage(url);
    return success;
}

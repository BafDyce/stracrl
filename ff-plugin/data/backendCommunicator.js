function createBaseUrl(){
    var serverInfo = require("./server.json");

    var url = '';
    if ( serverInfo.hasOwnProperty('protocol') && serverInfo.protocol !== null ){
        url += serverInfo.protocol;
    } else {
        url += 'http';
    }
    url += '://'

    if ( serverInfo.hasOwnProperty('address') && serverInfo.address !== null ){
        url += serverInfo.address;
    } else {
        url += '127.0.0.1';
    }

    if ( serverInfo.hasOwnProperty('port') && serverInfo.port !== null ){
        url += ':' + serverInfo.port;
    }

    return url;
}
var baseUrl = createBaseUrl();

module.exports = {
    driverLookup: function(drivernames, callback){
        var Request = require("sdk/request").Request;

        Request({
          url: baseUrl + '/drivers',
          contentType: "application/json; charset=utf8",
          content: JSON.stringify({drivers: drivernames}),
          onComplete: function (response) {
            if ( response.status === 200 ){
                var responseData = JSON.parse(response.text);

                if ( responseData.hasOwnProperty('error') === false ){
                    callback(responseData);
                    return;
                }

                console.log('CRITICAL | Backend returned an error:'
                    + responseData.error);
            } else {
                console.log('WARN | Connection to server failed with status code ' + response.status);
            }
          }
        }).post();
    }
}

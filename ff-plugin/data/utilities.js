var utilities = {
    urlToBaseUrl: function urlToBaseUrl(fullUrl){
        var url = require("sdk/url").URL(fullUrl);
        if ( url.port !== undefined && url.port !== null ){
            return url.protocol + '//' + url.host + ':' + url.port;
        }

        return url.protocol + '//' + url.host;
    },
    escapeHTML: function escapeHTML(text) {
        // NOTE: when updating this function, remember to also update panel-page-script.js !!
        // https://stackoverflow.com/questions/1787322/htmlspecialchars-equivalent-in-javascript/4835406#4835406
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    }
};

try {
    // utilities.js was imported via require()
    module.exports = utilities;
} catch(e) {
    // utilities.js was loaded onto a page script
}

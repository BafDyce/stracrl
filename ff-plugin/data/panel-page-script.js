// panel-page-script.js
var powerState = true;

addon.port.on('panelOpenPageChange', function(data){
    if ( data.hasOwnProperty('url') && data.hasOwnProperty('enabledPages') ){
        if ( isEnabledPage(data.url, data.enabledPages) ){
            setAddRemoveButton('remove', data.url);
        } else {
            setAddRemoveButton('add', data.url);
        }
    }
});

sendMessage('panelGetConfig', undefined, function(config){
    if ( config.hasOwnProperty('onOffState') ){
        setOnOffState(config.onOffState, false);
    }

    if ( config.hasOwnProperty('enabledPages') ){
        setEnabledPages(config.enabledPages);
    }
});

// END OF MAIN
function sendMessage(type, payload, callback){
    if ( typeof type === 'string' ){
        if ( typeof payload !== 'undefined' ){
            addon.port.emit(type, payload);
        } else {
            addon.port.emit(type);
        }

        if ( typeof callback === 'function'){
            addon.port.once(type + 'Reply', function(replyPayload){
                callback(replyPayload);
            });
        }
    }
}

function isEnabledPage(url, enabledPages){
    for ( var ii = 0; ii < enabledPages.length; ii++ ){
        if ( url.indexOf(enabledPages[ii]) == 0 ){
            return true;
        }
    }

    return false;
}

function togglePowerState(){
    setOnOffState(!powerState, true);
}

function setOnOffState(state, informAddOn){
    if ( typeof state === 'boolean'){
        var powerButton = document.getElementById('powerButton');
        var addRemoveButton = document.getElementById('addRemoveButton');
        if ( state ){
            powerButton.src = './images/power-on.png';
            powerButton.title = 'Turn stracrl off';
            addRemoveButton.disabled = false;
        } else {
            powerButton.src = './images/power-off.png';
            powerButton.title = 'Turn stracrl on';
            addRemoveButton.disabled = true;
        }

        // save new state internally
        powerState = state;

        if ( typeof informAddOn === 'boolean' && informAddOn ){
            // send message to AddOn-Script
            addon.port.emit('panelStateNotice', state);
        }
    }
}

function setAddRemoveButton(mode, url){
    var button = document.getElementById('addRemoveButton');
    if ( mode === 'add' ){
        button.innerHTML = 'Add current page.';
        button.onclick = function(){
            addRemovePage('add', url);
        };
    } else if ( mode === 'remove' ){
        button.innerHTML = 'Remove current page';
        button.onclick = function(){
            addRemovePage('remove', url);
        };
    } else {
        console.log('setAddRemoveButton: invalid mode (' + mode + ')');
    }

}

function addRemovePage(mode, url){
    var data = {mode: mode, url: url};
    sendMessage('panelAddRemovePage', data, function(reply){
        if ( reply.hasOwnProperty('success') && reply.success === true
                && reply.hasOwnProperty('enabledPages') && reply.hasOwnProperty('mode')){
            // update add/remove button
            var newMode = '';
            if ( reply.mode === 'add' ){
                newMode = 'remove';
            } else if ( reply.mode === 'remove' ){
                newMode = 'add';
            }
            setAddRemoveButton(newMode, url);

            // update list of enabled pages
            setEnabledPages(reply.enabledPages);
        }
    });
}

function setEnabledPages(enabledPages){
    if ( Object.prototype.toString.call( enabledPages ) === '[object Array]' ){
        var container = document.getElementById('enabledPagesContainer');
        clearNode(container);

        for( var ii = 0; ii < enabledPages.length; ii++){
            var page = enabledPages[ii];
            addEnabledPage(container, page);
        }
    }
}

function addEnabledPage(container, page){
    var element = document.createElement('div');
    element.setAttribute('class', 'pageListing');

    // url
    var text = document.createElement('font');
    text.innerHTML = escapeHtml(page);
    text.onclick = function(){ openPage(page); };
    text.setAttribute('class', 'interactive');
    element.appendChild(text);

    // delete button
    var delButton = document.createElement('img');
    delButton.src = './images/trash.png';
    delButton.setAttribute('class', 'interactive');
    delButton.onclick = function(){deleteEnabledPage(element)};
    element.appendChild(delButton);

    container.appendChild(element);
}

function deleteEnabledPage(pageElement){
    var pageToRemove = pageElement.childNodes[0].innerHTML;
    var data = {mode: 'remove', url: pageToRemove}
    sendMessage('panelAddRemovePage', data, function(response){
        if ( response.hasOwnProperty('success') && response.success === true){
            pageElement.remove();
        }
    });
}

function clearNode(node){
    // faster than node.innerHTML = '', see:
    // https://stackoverflow.com/questions/3955229/remove-all-child-elements-of-a-dom-node-in-javascript/3955238#3955238
    while( node.firstChild){
        node.removeChild(node.firstChild);
    }
}

function openPage(url){
    sendMessage('panelOpenPage', url, undefined);
}

// duplicate of utilities.js:escapeHTML() because we can't use require() in this context
function escapeHtml(text) {
  // https://stackoverflow.com/questions/1787322/htmlspecialchars-equivalent-in-javascript/4835406#4835406
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

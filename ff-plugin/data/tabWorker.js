// tabWorker.js
var sections = [
    {section: '/players', callback: function(){
        var options = {hrefCheck: 'playerdetails', nameIdx: 0, insertIdx: 0};
        processPage(options);
    }},
    {section: '/lapstat', callback: function(){
        var options = {hrefCheck: 'lapdetails', nameIdx: 1, insertIdx: 1};
        processPage(options);
    }}
];

tabWorkerMain();
// END OF MAIN

function tabWorkerMain(){
    doIfPluginEnabled( function(enabledPages){
        if ( Object.prototype.toString.call( enabledPages ) === '[object Array]' ){
            var uri = document.baseURI;
            self.port.emit('workerOpenPage', uri);
            for(var ii = 0; ii < enabledPages.length; ii++){
                if ( uri.indexOf(enabledPages[ii]) == 0){
                    run(uri, enabledPages[ii].length);
                    break;
                }
            }
        }
    });
}

function doIfPluginEnabled(callback){
    self.port.emit('workerGetConfig');
    self.port.once('workerGetConfigReply', function(config){
        if ( config.hasOwnProperty('onOffState') && config.onOffState === true
                &&  config.hasOwnProperty('enabledPages') ){
            callback(config.enabledPages);
        }
    });
}

function run(uri, idx){
    var sectionsLength = sections.length;
    for(var ii = 0; ii < sectionsLength; ii++){
        if ( uri.indexOf(sections[ii].section) == idx ){
            sections[ii].callback();
            break;
        }
    }
}

function processPage(options){
    if ( options.hasOwnProperty('hrefCheck')
            && options.hasOwnProperty('nameIdx')
            && options.hasOwnProperty('insertIdx')){
        var headerRow = document.getElementsByTagName('thead')[0].children[0];
        if ( headerRow !== undefined ){
            headerRow.setAttribute('id', 'headerRow');

            var names = retrieveDriverNamesFromPage('clickableRow', options.hrefCheck, options.nameIdx);
            queryDriverNames(names, function(result){
                var length = result.length;
                if ( length > 0 ){
                    // create required cells in header row
                    var headerRow = document.getElementById('headerRow');
                    if ( headerRow !== undefined ){
                        var cell = headerRow.insertCell(options.insertIdx);
                        cell.outerHTML = '<th>GTE</th>';
                        var cell = headerRow.insertCell(options.insertIdx);
                        cell.outerHTML = '<th>GT3</th>';

                        // populate cells
                        for( var ii = 0; ii < length; ii++){
                            displayDriver(result[ii], options.insertIdx);
                        }
                    } else {
                        console.log('[ERROR] No element with id \'headerRow\' found.');
                    }
                }
            });
        } else {
            console.log('[ERROR] No <thead>-Element found.');
        }
    }
}

function displayDriver(driver, idx){
    // get rows with class drivername (each driver-car combo has one row)
    var rows = document.getElementsByClassName(driver.name);
    for ( var ii = 0; ii < rows.length; ii++){
        var row = rows[ii];

        // add cells if they are still missing
        // necessary because if a driver is in Gt3 & gte there'll be
        // two emtries for the same row(s)
        if ( row.getAttribute('class').indexOf('stracrl') < 0 ){
            var cell = row.insertCell(idx);
            var cell = row.insertCell(idx);
        }

        var cell = undefined;
        var series = driver.series;
        if ( series === 'gt3' ){
            cell = row.children[idx];
        } else if ( series === 'gte'){
            cell = row.children[idx + 1];
        }

        populateCell(cell, driver);
        var newClass = row.getAttribute('class') + ' stracrl';
        row.setAttribute('class', newClass);
    }
}

function populateCell(cell, driverinfo){
    if ( cell !== undefined
            && driverinfo !== undefined
            && driverinfo.hasOwnProperty('region')
            && driverinfo.hasOwnProperty('division')
            && driverinfo.hasOwnProperty('series')){
        var innerHTML = '<nobr>';

        if ( driverinfo.region === 'eu'){
            innerHTML += '<img src="' + self.options.imgRegionEu + '" />';
        } else if (driverinfo.region === 'na'){
            innerHTML += '<img src="' + self.options.imgRegionNa + '" />';
        }

        if ( driverinfo.series === 'gt3'){
            if ( driverinfo.division === 'pro'){
                innerHTML += '<img src="' + self.options.imgGt3Pro + '" />';
            } else if (driverinfo.division === 'pro-am'){
                innerHTML += '<img src="' + self.options.imgGt3ProAm + '" />';
            } else if ( driverinfo.division === 'am'){
                innerHTML += '<img src="' + self.options.imgGt3Am + '" />';
            }
        } else if (driverinfo.series === 'gte'){
            innerHTML += '<img src="' + self.options.imgGte + '" />';
        }

        innerHTML += '</nobr>';
        cell.innerHTML = innerHTML;
    }
}

function queryDriverNames(drivernames, callback){
    // We can't do a direct call to the server from the contentScript because FF
    // blocks this request due to Cross-Site stuff. Therefore, we need to transmit
    // the data to our add-on script, which makes the request to the server and
    // forwards back the result
    // Serialization, etc. will be done by the add-on script
    self.port.emit("workerDriversLookup", drivernames);
    self.port.once("workerDriversLookupReply", function(response){
        callback(response);
    });
}

// retrieve driver names and add class to each row for quick access later
function retrieveDriverNamesFromPage(className, hrefCheck, idx){
    var rows = document.getElementsByClassName(className);

    var names = [];
    for ( var ii = 0; ii < rows.length; ii++){
        var row = rows[ii];
        // ignore all rows of other tables
        if ( row.getAttribute('href').indexOf(hrefCheck) !== -1 ){
            var name = row.children[idx].innerHTML;
            row.className += ' ' + utilities.escapeHTML(name);
            names.push(name);
        }
    }

    return names;
}

var ss = require("sdk/simple-storage");

//var ConfigurationHandler = function(){
function ConfigurationHandler(){
    this.onOffState = false;
    this.enabledPages = [];
    this.load();
}

ConfigurationHandler.prototype.getConfig = function(){
    return {onOffState: this.onOffState, enabledPages: this.enabledPages};
}

ConfigurationHandler.prototype.setOnOffState = function(state){
    if ( typeof state === 'boolean' ){
        this.onOffState = state;
        this.save();
    }
}

ConfigurationHandler.prototype.getOnOffState = function(){
    return this.onOffState;
}

ConfigurationHandler.prototype.getEnabledPages = function(){
    return this.enabledPages;
}

ConfigurationHandler.prototype.addPage = function(pageToAdd){
    var idx = this.enabledPages.indexOf(pageToAdd);
    if ( idx === -1 ){
        this.enabledPages.push(pageToAdd);
        this.save();
        return true;
    }

    return false;
}

ConfigurationHandler.prototype.removePage = function(pageToRemove){
    var idx = this.enabledPages.indexOf(pageToRemove);
    if ( idx >= 0 ){
        this.enabledPages.splice(idx, 1);
        this.save();
        return true;
    }

    return false;
}

ConfigurationHandler.prototype.load = function(){
    var changed = false;
    this.onOffState = (ss.storage.onOffState !== undefined ? ss.storage.onOffState : true);
    if ( typeof this.onOffState !== 'boolean' ){
        console.log('ss.storage.onOffState was \'' + this.onOffState + '\'. Setting it to true');
        this.onOffState = true;
        changed = true;
    }

    this.enabledPages = (ss.storage.enabledPages !== undefined ? ss.storage.enabledPages : []);
    if ( Object.prototype.toString.call( this.enabledPages ) !== '[object Array]' ){
        console.log('ss.storage.enabledPage was \'' + this.enabledPages + '\'. Setting to empty array.');
        this.enabledPages = [];
        changed = true;
    }

    if ( changed ){
        this.save();
    }
}

ConfigurationHandler.prototype.save = function(){
    console.log('\n=======================================\nSaving configuration');
    console.log('onOffState = ' + this.onOffState);
    ss.storage.onOffState = this.onOffState;
    console.log('enabledPages = ' + this.enabledPages);
    ss.storage.enabledPages = this.enabledPages;
    console.log('Saving done\n=======================================');
}

// if we'd directly export new ConfigurationHandler() alls it's members would be read-only. dunno why
//module.exports = new ConfigurationHandler();
module.exports = ConfigurationHandler;
